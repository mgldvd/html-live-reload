/* eslint semi: "error" */
/* jshint node: true */
/* jshint esversion: 6 */

'use strict';

const colors = require('colors');

const fruits = ['🍇', '🍈', '🍉', '🍊', '🍋', '🍌', '🍍', '🥭', '🍎', '🍏', '🍐', '🍑', '🍒', '🍓', '🫐', '🥝', '🍅', '🫒', '🥥', '🥑', '🍆', '🥔', '🥕', '🌽', '🌶', '🥒', '🫑', '🥬', '🥦', '🧄', '🧅', '🍄', '🥜', '🌰', '🖕'];
let rand = Math.floor(Math.random()*fruits.length);
let banner = new Array(13);
banner = banner.fill(fruits[rand]).join(' ');

console.log(colors.white(`
${banner}

   Welcome

   ⚠️  CHECKLIST Remember to:
      ◻️  - Delete folder ${colors.cyan('.git')}
      ◻️  - edit file ${colors.cyan('.gitignore')}

   Now run: ` +  colors.yellow('gulp dev') + `
   and after you finish coding
   remember to run: ` +  colors.yellow('gulp dist') + `

${banner}
`));
