/* global console */
/* eslint no-undef: "error", semi: 2 */

'use strict';

import {
  src,
  dest,
  parallel,
  series,
  watch
} from 'gulp';

import notify from 'gulp-notify'
import presetEnv from 'postcss-preset-env'
import hexrgba from 'postcss-hexrgba';
import autoprefixer from 'autoprefixer';
import cssnano from 'cssnano';
import unique from 'postcss-unique-selectors';
import duplicates from 'postcss-discard-duplicates';
import sourcemaps from 'gulp-sourcemaps';
import plumber from 'gulp-plumber';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import postcss from 'gulp-postcss';
import cssvariables from 'postcss-css-variables';
import concat from 'gulp-concat';
import rename from 'gulp-rename';
import browserSync from 'browser-sync';

const sass = gulpSass(dartSass);

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  SASS
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

function onErrorSass (err) {
  var rexUrl = /[^\\/]+$/;
  var pattern = rexUrl.exec(err.file);
  notify.onError({
      title  : ' ::::::: Sass Error ::::::: ',
      message: `
      code ERROR
      File    :  ${pattern}
      Line    :  ${err.line}
      Message :  ${err.messageOriginal}
    `,
    sound: 'Beep'
  })(err);
  this.emit('end');
}

var postcssPlugins = [
  presetEnv(),
  hexrgba(),
  autoprefixer(),
  cssvariables(),
  cssnano({
    discardComments: true
  }),
  unique(),
  duplicates()
];

function compileCss () {
  return src([
    './src/sass/**/*.scss',
  ], { base: './src/' })
  .pipe(sourcemaps.init())
  .pipe(plumber({ errorHandler: onErrorSass }))
  .pipe(concat('main.css'))
  .pipe(sass())
  .pipe(postcss(postcssPlugins))
  .pipe(sourcemaps.write('./'))
  .pipe(rename({
    dirname: "css",
  }))
  .pipe(dest('./.tmp/'));
}

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  JS
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

import babel from 'gulp-babel';
import strip from 'gulp-strip-comments';
// import plumber from 'gulp-plumber';

function onErrorJs (err) {
  notify.onError({
    title  : ' ::::::: JS Error ::::::: ',
    message: `
      ${err}
    `,
    sound  : 'Beep'
  })(err);
  this.emit('end');
}

function compileJs () {
  return src([
    './src/js/**/*.js'
  ], {base: './src/'})
  .pipe(plumber({errorHandler: onErrorJs}))
  .pipe(babel())
  .pipe(strip())
  .pipe(dest('./.tmp/'));
}

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Html
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

import fileinclude from 'gulp-file-include';
// import plumber from 'gulp-plumber';


function compileHtml () {
  return src([
      './src/*.html',
      './src/*.php'
    ])
    .pipe(plumber())
    .pipe(fileinclude())
    .pipe(dest('./.tmp/'));
}


/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Sync
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
*/

function syncTmp() {
  return src([
    './src/**/*',
    '!./src/sass/*',
    '!./src/js/*'
  ]).pipe(dest('./.tmp/'));
}

function syncImg() {
  return src(['./src/images/**/*'])
  .pipe(dest('./.tmp/images/'));
}

/**
 * ::::::: Hidden Dist
 */

import fs from 'fs-extra';

function hiddenDist() {
  return fs.outputFile('./dist/.hidden', '');
}



/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Notifications
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 **/

const log = console.log.bind(console);
import colors from 'colors';
import lolcatjs from 'lolcatjs';

function notifyDoneHtml(done) {
  log(colors.black.bold.bgGreen('      ▲ ▲ ▲ ::::::: html udpate ::::::: ▲ ▲ ▲       '));
  done();
}

function notifyDonePhp(done) {
  log(colors.black.bold.bgGreen('      ▲ ▲ ▲ ::::::: php udpate ::::::: ▲ ▲ ▲        '));
  done();
}

function notifyDoneCss(done) {
  log(colors.white.bold.bgMagenta('      ▲ ▲ ▲ ::::::: css udpate ::::::: ▲ ▲ ▲        '));
  done();
}

function notifyDoneJs(done) {
  log(colors.black.bold.bgCyan('      ▲ ▲ ▲ ::::::: js udpate ::::::: ▲ ▲ ▲         '));
  done();
}

function notifyDoneJson(done) {
  log(colors.black.bold.bgYellow('      ▲ ▲ ▲ ::::::: json udpate ::::::: ▲ ▲ ▲       '));
  done();
}

function notifyDoneImg(done) {
  log(colors.black.bold.bgBlue('      ▲ ▲ ▲ ::::::: Sync Images ::::::: ▲ ▲ ▲       '));
  done();
}

function notifyDoneFiles(done) {
  lolcatjs.fromString(`
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
· · · · · · · · · · · · · · · · · · · · · · · · · ·
· · · · · · · Dist DONE · · · · · · · · · · · · · ·
· · · · · · · · · · · · · · · · · · · · · · · · · ·
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
  `);
  log(colors.magenta.bgYellow('              Files ready on dist/ folder          \n'));
  done();
}

function notifyDoneDist(done) {
  lolcatjs.fromString(`
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
· · · · · · · · · · · · · · · · · · · · · · · · · ·
· · · · · · · Dist DONE · · · · · · · · · · · · · ·
· · · · · · · · · · · · · · · · · · · · · · · · · ·
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
  `);
  done();
}
/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Del
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
*/
import clean from 'gulp-clean';

function delTemp (done) {
  return src(['./.tmp/*']).pipe(clean());
  done();
};

function delSass (done) {
  return src(['./.tmp/**/*.css']).pipe(clean());
  done();
};

function delJs (done) {
  return src(['./.tmp/**/*.js']).pipe(clean());
  done();
};

function delHtml (done) {
  return src(['./.tmp/**/*.html', '!./.tmp/index.html']).pipe(clean());
  done();
};

function delJson (done) {
  return src(['./.tmp/**/*.json']).pipe(clean());
  done();
};

function delImg (done) {
  return src(['./.tmp/images']).pipe(clean());
  done();
};

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Watch
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 **/

function watchFiles() {
  watch(['./src/**/*.scss'],   series(delSass, compile, reload, notifyDoneCss));
  watch('./src/**/*.js',       series(delJs,   compile, reload, notifyDoneJs));
  watch('./src/**/*.html',     series(delHtml, compile, reload, notifyDoneHtml));
  watch('./src/**/*.json',     series(delJson, compile, reload, notifyDoneJson));
  watch('./src/images/**/*',   series(delImg,  syncImg, reload, notifyDoneImg));
}

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Server
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
*/


function serverHtml() {
  browserSync.init({
    port  : 8888,
    ui    : false,
    open  : true,
    server: {
      baseDir  : './.tmp/',
      directory: true
    }
  });
}

function reload(done) {
  browserSync.reload();
  done();
}



/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Run
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
*/

const compile = series(
  syncTmp,
  parallel(
    compileCss,
    compileJs
  ),
  compileHtml,
);

const run = series(
  compile,
  parallel(
    serverHtml,
    watchFiles
  ),
  hiddenDist
);

export default run;


